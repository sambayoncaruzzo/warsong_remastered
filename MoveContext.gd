extends Node

signal Celda_Clickeada_Posicion(Posicion:Vector2)
signal Mover_Unidad_Posicion(Posicion:Vector2)

func Celda_Clickeada_Posicion_Context(Posicion:Vector2):
	var selectedUnit = GroupManager.get_selected_unit(self)
	if selectedUnit != null:
		emit_signal("Mover_Unidad_Posicion", Posicion)

func Finalizacion_Movimiento(unidad:Node)->void:
	GroupManager.unselect_unit(unidad.get_tree())
	%Contexts.SetActiveContextGeneral()
	print(GroupManager.get_selected_unit(unidad))
