extends Node

@onready var Contexts:Array[Node] = [%GeneralContext,%MoveContext]

@onready var Active_Context:Node = Contexts[0]

func SetActiveContextGeneral()->void:
	Active_Context = Contexts[0]
	print("Seteando Contexto General")
	
func SetActiveContextMove()->void:
	Active_Context = Contexts[1]
	print("Seteando Contexto Mover")

func Manager_Unidad_Seleccionada(unidad:Node):
	if Active_Context.has_method("Unidad_Seleccionada_Context"):
		Active_Context.Unidad_Seleccionada_Context(unidad)

func Manager_Celda_Seleccionada(Posicion:Vector2):
	if Active_Context.has_method("Celda_Clickeada_Posicion_Context"):
		Active_Context.Celda_Clickeada_Posicion_Context(Posicion)


