class_name GroupManager
extends RefCounted

const UNIDAD_SELECCIONADA = "unidad_seleccionada";

static func selectUnit(selectedNode : Node) -> void:
	clearGroup(UNIDAD_SELECCIONADA, selectedNode.get_tree())
	selectedNode.add_to_group(UNIDAD_SELECCIONADA)

static func clearGroup(groupName : StringName, tree : SceneTree) -> void:
	var nodes = tree.get_nodes_in_group(groupName)
	for node in nodes:
		node.remove_from_group(UNIDAD_SELECCIONADA)

static func get_selected_unit(node : Node) -> Node:
	var nodes = node.get_tree().get_nodes_in_group(UNIDAD_SELECCIONADA)
	if nodes.size() > 0:
		return nodes[0]
	return null

static func unselect_unit(tree : SceneTree) -> void:
	clearGroup(UNIDAD_SELECCIONADA, tree)
