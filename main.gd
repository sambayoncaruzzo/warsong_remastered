extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and !event.pressed:
			
			var coordenada_celda = $Grilla.local_to_map(event.position)
			
			var centro_celda_pixeles = $Grilla.map_to_local(coordenada_celda)
			
			%Contexts.Manager_Celda_Seleccionada(centro_celda_pixeles)
