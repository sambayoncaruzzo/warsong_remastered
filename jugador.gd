extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	%MoveContext.connect("Mover_Unidad_Posicion", Mover_Unidad)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and !event.pressed:
			%Contexts.Manager_Unidad_Seleccionada(self)

func Mover_Unidad(Posicion:Vector2)->void:
	self.position = Posicion 
	%MoveContext.Finalizacion_Movimiento(self)
