extends Node
signal Unidad_Seleccionada(node:Node)

func Unidad_Seleccionada_Context(unidad:Node):
	GroupManager.selectUnit(unidad)
	emit_signal("Unidad_Seleccionada", unidad)
	
	
