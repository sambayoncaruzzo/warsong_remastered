extends PopupMenu

# Called when the node enters the scene tree for the first time.
func _ready():
	%GeneralContext.connect("Unidad_Seleccionada", _on_jugador_open_popup_menu)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_jugador_open_popup_menu(node:Node):
	self.visible = true
	self.position = node.position
	pass

func _on_id_pressed(id):
	var Item_Text = self.get_item_text(id)
	if Item_Text == "Mover":
		%Contexts.SetActiveContextMove()
	pass
